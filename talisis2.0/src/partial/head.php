<meta charset="utf-8">
    <meta name="description" content="Talisis ayudamos a enriquecer el talento de alta calidad de capacitación">
    <meta name="keywords" content="de acuerdo a las decesidades especificas de tu empresa.">
    <meta name="author" content="Produccion a tu empresa">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- SEO Metatags -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Metodikat TI">

    <!-- Structured data -->
    <?php include_once('src/partial/seo/structured-data.php'); ?>

    <!-- Googlebot -->
    <?php include_once('src/partial/seo/googlebot.php'); ?>

    <!-- Facebook Pixel Code -->
    <?php include_once('src/partial/seo/fb-pixel.php'); ?>

    <!-- Title -->
    <title>Talisis: Learning & Development</title>
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Preloader -->
    <link href="assets/css/preloader.css" rel="stylesheet" />
    <script src="assets/js/preloader.js"></script>

    <!-- Bootstrap -->
    <link href="assets/js/bootstrap-4.0.0-beta.2-dist/css/bootstrap.min.css" rel="stylesheet" />

    <!-- CSS -->
    <link href="assets/css/main.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/bxslider/jquery.bxslider.min.css">
    <link rel="stylesheet" type="text/css" href="assets/js/modal-video/css/modal-video.min.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- AngularJS -->
    <!--    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js"></script>-->
    <!--    <script src="app/app.js"></script>-->

    <!-- Analytics code -->
    <?php include_once('src/partial/seo/analytics.php'); ?>