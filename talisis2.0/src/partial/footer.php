<div class="container-fluid contacto" id="contacto">
    <div class="container">
        <p class="title text-center">¿Estás listo para hacer más productiva a tu empresa?</p>
        <p class="content text-center">Ingresa tus datos de contacto en el siguiente formulario y nosotros nos pondremos en contacto:</p>
        <br>
        <form class="row" action="send.php" method="POST" enctype="multipart/form-data" id="form_contacto">
            <div class="col-md-6">
                <input value="" placeholder="Nombre completo" name="nombre_completo" required="required">
            </div>
            <div class="col-md-6">
                <input value="" placeholder="Email" name="email" required="required">
            </div>
            <div class="col-md-6">
                <input value="" placeholder="Teléfono (10 dígitos)" name="telefono" required="required">
            </div>
            <div class="col-md-6">
                <input value="" placeholder="Empresa" name="empresa" required="required">
            </div>
            <div class="col-md-12">
                <select required="required" name="estado">
                    <option value="">Estado de procedencia</option>
                    <option value="Aguascalientes">Aguascalientes</option>
                    <option value="Baja California">Baja California</option>
                    <option value="Baja California Sur">Baja California Sur</option>
                    <option value="Campeche">Campeche</option>
                    <option value="Chiapas">Chiapas</option>
                    <option value="Chihuahua">Chihuahua</option>
                    <option value="Ciudad de México">Ciudad de México</option>
                    <option value="Coahuila">Coahuila</option>
                    <option value="Colima">Colima</option>
                    <option value="Durango">Durango</option>
                    <option value="Guanajuato">Guanajuato</option>
                    <option value="Guerrero">Guerrero</option>
                    <option value="Hidalgo">Hidalgo</option>
                    <option value="Jalisco">Jalisco</option>
                    <option value="México">México</option>
                    <option value="Michoacán">Michoacán</option>
                    <option value="Morelos">Morelos</option>
                    <option value="Nayarit">Nayarit</option>
                    <option value="Nuevo León">Nuevo León</option>
                    <option value="Oaxaca">Oaxaca</option>
                    <option value="Puebla">Puebla</option>
                    <option value="Querétaro">Querétaro</option>
                    <option value="Quintana Roo">Quintana Roo</option>
                    <option value="San Luis Potosí">San Luis Potosí</option>
                    <option value="Sinaloa">Sinaloa</option>
                    <option value="Sonora">Sonora</option>
                    <option value="Tabasco">Tabasco</option>
                    <option value="Tamaulipas">Tamaulipas</option>
                    <option value="Tlaxcala">Tlaxcala</option>
                    <option value="Veracruz">Veracruz</option>
                    <option value="Yucatán">Yucatán</option>
                    <option value="Zacatecas">Zacatecas</option>
                </select>
            </div>
            <div class="col-md-12">
                <textarea placeholder="Escribe aquí tu mensaje o asunto" name="comentario" required="required"></textarea>
            </div>
            <div class="col-md-12 text-center">
                <button class="button_enviar">ENVIAR</button>
            </div>
        </form>
    </div>
</div>


<footer class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-xl-4 col-lg-4">
                <a class="link_navegacion" href="index.php">HOME</a><br>
                <a class="link_navegacion" href="index.php#que_es_talisis">TALISIS: LEARNING & DEVELOPMENT</a><br>
                <a class="link_navegacion" href="#contacto">CONTACTO</a><br>
                <a class="link_navegacion" href="https://portal.office.com" target="_blank">ACCESO COLABORADORES</a>
            </div>
            <div class="col-sm-6 col-md-4 col-xl-4 col-lg-4">
                <div class="w-100">
                    <p class="link_navegacion no_hover">TELÉFONO:</p>
                    <a class="info_link_navegacion" href="tel:+ 52 123-456-7890">+ 52 123-456-7890</a>
                </div>
                <div class="w-100">
                    <p class="link_navegacion no_hover m-0">OFICINA CENTRAL:</p>
                    <p class="address_navegacion">Matamoros #430 Pte. Col. Centro, Monterrey, N. L.</p>
                </div>
                <div class="w-100">
                    <p class="link_navegacion no_hover">EMAIL:</p>
                    <a class="info_link_navegacion" href="mailto:infold@talisis.com">infold@talisis.com</a>
                </div>
            </div>
            <div class="col-sm-12 col-md-4 col-xl-4 col-lg-4">
                <div class="row">
                    <div class="col-4 col-sm-4 col-md-4 col-xl-4 col-lg-4 text-center">
                        <a href="https://www.facebook.com/Talisis-Learning-Development-829855763849974/" target="_blank" class="redes_sociales">
                            <img class="social_logo" src="assets/img/fb-off-icon.png" alt="facebook">
                        </a>
                    </div>
                    <div class="col-4 col-sm-4 col-md-4 col-xl-4 col-lg-4 text-center">
                        <a href="https://www.linkedin.com/company/27142930/" target="_blank" class="redes_sociales">
                            <img class="social_logo" src="assets/img/in-off-icon.png" alt="instagram">
                        </a>
                    </div>
                    <div class="col-4 col-sm-4 col-md-4 col-xl-4 col-lg-4 text-center">
                        <a href="https://www.instagram.com/talisisld/" target="_blank" class="redes_sociales">
                            <img class="social_logo" src="assets/img/instagram.png" alt="gmail">
                        </a>
                    </div>
                    <div class="col-md-12 text-center">
                        <br><br>
                        <img src="assets/img/drivechange-footer-id.png" class="img-fluid" alt="footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="col-md-12 subfooter">
    <div class="container text-center">
        <p class="content">© 2017 Talisis: Learning & Development. Todos los derechos reservados. Diseñado por <a href="http://futurite.com/" target="_blank" class="futurite">Futurite</a>. Desarrollado por <a href="https://www.metodika.mx/" target="_blank" class="metodika">METODIKA</a>.</p>
    </div>
</div>