<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="assets/js/jquery.bxslider.min.js"></script>

<!-- Bootstrap -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="assets/js/bootstrap-4.0.0-beta.2-dist/js/bootstrap.min.js"></script>

<!-- Bootbox -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

<!-- Chat -->
﻿<script src="https://app.five9.com/consoles/SocialWidget/five9-social-widget.min.js"></script>

<script src="assets/js/modal-video/js/jquery-modal-video.js"></script>
<script src="assets/js/mapresponsive.js"></script>
<script src="assets/js/chat.js"></script>
<script src="assets/js/index.js"></script>