<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="index.php">
        <img src="assets/img/home/lnd-id.svg" class="nav_icon" alt="icono">
    </a>
    <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto ml-auto w-100 justify-content-end">
            <li class="nav-item">
                <a class="nav-link text-uppercase" href="nosotros.php">NOSOTROS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-uppercase" href="educacion-continua.php">Aprendizaje y Desarrollo</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-uppercase" href="educacion-ejecutiva.php">EDUCACIÓN EJECUTIVA</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-uppercase" href="talisis-consultoria.php">Consultoría</a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-uppercase" href="#contacto">CONTACTO</a>
            </li>
            <li class="nav-item">
                <a class="nav-link button_cliente text-uppercase" href="sitio-construccion.php">YA SOY CLIENTE</a>
            </li>
<!--            <li class="nav-item">-->
<!--                <a class="nav-link button_email" href="https://portal.office.com" target="_blank">EMAIL</a>-->
<!--            </li>-->
        </ul>
    </div>
</nav>

