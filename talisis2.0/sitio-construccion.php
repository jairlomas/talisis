<!DOCTYPE html>
<html lang="es" ng-app="MetodikaTI">
<head>
    <?php include_once('src/partial/head.php'); ?>
</head>

<body class="construccion">

<!-- Preloader -->
<div class="preloader"></div>

<!-- Page header section -->
<?php //include_once('src/partial/header.php'); ?>

<!-- All page content goes inside this div -->
<div id="pageContent" class="main-wrapper">


    <section class="container-fluid">
        <div class="container text-center">
            <div class="row align-items-center full_height">
                <div class="col">
                    <p class="little_title">SITIO EN</p>
                    <p class="big_title">CONSTRUCCIÓN</p>
                    <p class="extra_little_title">POR FAVOR, VISITANOS MÁS TARDE</p>
                    <br>
                    <a onclick="window.history.back();" >REGRESAR AL SITIO</a>
                </div>
            </div>
        </div>
    </section>


    <!-- Page footer section -->
    <?php //include_once('src/partial/footer.php'); ?>

</div>


<!-- Scripts -->
<?php include_once('src/partial/js.php'); ?>

</body>
</html>
