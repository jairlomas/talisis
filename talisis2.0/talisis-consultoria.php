<!DOCTYPE html>
<html lang="es" ng-app="MetodikaTI">
<head>
    <?php include_once('src/partial/head.php'); ?>
</head>

<body>

<!-- Preloader -->
<div class="preloader"></div>

<!-- Page header section -->
<?php include_once('src/partial/header.php'); ?>

<!-- All page content goes inside this div -->
<div id="pageContent" class="main-wrapper consultoria">


    <div class="container-fluid top_header_interior bg-consultoria text-center">
        <div class="container">
            <p class="title">CONSULTORÍA</p>
        </div>
    </div>


    <div class="container-fluid crecimiento_empresa text-center">
        <div class="container">
            <h2 class="title">Asesoría profesional para tu empresa</h2>
            <p class="content ">En Talisis Learning & Development comprendemos que cada empresa tiene procesos y manejos únicos, por eso ofrecemos servicios de consultoría con base en las necesidades de tu empresa.</p>
            <h2 class="title2">Análisis en enfoque empresarial por fases para la detección de oportunidades de tu empresa</h2>
            <br><br>
            <div class="row">
                <div class="col-12 col-md">
                    <img src="assets/img/consultoria/paso1-consultoria-gpc.png" class="img-fluid" alt="Organización"><p class="pt-4 ">Identifica los objetivos a lograr y necesidad de la organización.</p>
                </div>
                <div class="col-12 col-md">
                    <img src="assets/img/consultoria/paso2-consultoria-gpc.png" class="img-fluid" alt="gpc"><p class="pt-4 ">Diagnóstico de las áreas de oportunidad, relación de identificadores, procesos, tareas y actores; brechas y hoja de rutas a desarrollar.</p>
                </div>
                <div class="col-12 col-md">
                    <img src="assets/img/consultoria/paso3-consultoria-gpc.png" class="img-fluid" alt="Naturaleza"><p class="pt-4 ">De acuerdo a la naturaleza de la empresa, se desarrolla el plan de acciones a ejecutar y los indicadores a tener en cuenta.</p>
                </div>
                <div class="col-12 col-md">
                    <img src="assets/img/consultoria/paso4-consultoria-gpc.png" class="img-fluid" alt="Paso"><p></p>
                </div>
                <div class="col-12 col-md">
                    <img src="assets/img/consultoria/paso5-consultoria-gpc.png" class="img-fluid" alt="Procso"><p class="pt-4 ">El proceso de consultoría y entrega de plan de acciones que oriente a la organización a dar el seguimiento oportuno.</p>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid crecimiento_empresa">
        
    </div>


    <div class="container-fluid prodution ">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <h2 class="title">PRODUCTOS</h2>
                </div>
            </div>
        </div>
        
        <div class="row">
            
            <div class="col-md-4 sec1">
                <div class="">
                    <div class="offset-2 col-9">
                        <h2 class="title">Aprendizaje y Desarrollo Organizacional</h2>
                        <div class="list">
                            <ul>
                                <li>Universidad y Escuelas Corporativas</li>
                                <li>Diseño Institucional</li>
                                <li>Desarrollo de Recursos y Aprendizaje</li>
                                <li>Desarrollo de cursos en línea</li>
                                <li>Consultoría en Desarrollo Organizacional</li>
                                <li>Gestión de Competencias</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-md-4 sec2">
                <div class="">
                    <div class="offset-2 col-9">
                        <h2 class="title">Consultoría de Negocios</h2>
                        <div class="list">
                            <ul>
                                <li>Consultoría de Innovación</li>
                                <li>Planeación Estratégica</li>
                                <li>Coaching directivo</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-md-4 sec3">
                <div class="">
                    <div class="offset-2 col-9">
                        <h2 class="title">Consultoría en Calidad y Mejora Continua</h2>
                        <div class="list">
                            <ul>
                                <li>Consultoría de procesos de calidad</li>
                                <li>Procesos de certificación en ISO</li>
                                <li>Consultoría en mejora continua</li>
                                <li>Consultoría en Responsabilidad Social Corporativa</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>




    <!-- Page footer section -->
    <?php include_once('src/partial/footer.php'); ?>

</div>



<!-- Scripts -->
<?php include_once('src/partial/js.php'); ?>

</body>
</html>
