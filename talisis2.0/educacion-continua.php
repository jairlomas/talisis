<!DOCTYPE html>
<html lang="es" ng-app="MetodikaTI">
<head>
    <?php include_once('src/partial/head.php'); ?>
</head>

<body>

<!-- Preloader -->
<div class="preloader"></div>

<!-- Page header section -->
<?php include_once('src/partial/header.php'); ?>

<!-- All page content goes inside this div -->
<div id="pageContent" class="main-wrapper">


    <div class="container-fluid top_header_interior bg-educacion_continua text-center">
        <div class="container">
            <p class="title">EDUCACIÓN CONTINUA</p>
        </div>
    </div>



    <div class="soluciones_corporativas container-fluid text-center">
        <div class="container">
            <p class="title">Soluciones Corporativas de Aprendizaje</p>
            <p class="description text-center">Talisis: Learning &amp; Development sabe que tu empresa no puede detenerse, contamos con diferentes programas, diplomados y talleres para brindar Educación Continua a tus colaboradores.</p>
            <p class="subtitle_content">Algunos proyectos de Educación Continua en los que hemos trabajado exitosamente son:</p>
            <br>
            <div class="row">
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                    <img class="logo_bussiness" src="assets/img/home/claut-educacion-continua-img.png" alt="Negocios">
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                    <img class="logo_bussiness" src="assets/img/home/multimedios-educacion-continua-img.png" alt="Negocios Logo1">
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                    <img class="logo_bussiness" src="assets/img/home/cemex-educacion-continua-img.png" alt="bussiness">
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                    <img class="logo_bussiness" src="assets/img/home/abinbev-educacion-continua-img.png" alt="bussiness-logo">
                </div>
            </div>
        </div>
    </div>
    <div class="soluciones_corporativas_aprendizaje section_content text-center bg-gray">

        <div class="container">
            <p class="title">Soluciones Corporativas de Aprendizaje</p>
            <p class="description text-center">Talisis tiene como objetivo fortalecer el talento de tu empresa, por lo que nos especializamos en crear planes de capacitación enfocadas en el desarrollo de tus colaboradores.</p>
            <p class="subtitle_content">Brindamos capacitación en las siguientes áreas:</p>
            <br>

            <div class="row">
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/negocios-educacion-ejecutiva-icon.png" alt="Educación Ejecutiva">
                    <p class="title_area">NEGOCIOS</p>
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/comercial-educacion-ejecutiva-icon.png" alt="Comercial">
                    <p class="title_area">COMERCIAL</p>
                    <p class="little_area">VENTAS Y MERCADOTECNIA</p>
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/calidad-educacion-ejecutiva-icon.png" alt="Ventas y Mercadotecnia">
                    <p class="title_area">CALIDAD Y MEJORA CONTINUA</p>
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/comercial-educacion-ejecutiva-icon.png" alt="Educación">
                    <p class="title_area">RH Y EDUCACIÓN</p>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/finanzas.png" alt="Finanzas">
                    <p class="title_area">FINANZAS</p>
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/construccion-educacion-ejecutiva-icon.png" alt="Construcción">
                    <p class="title_area">INDUSTRIA DE LA CONSTRUCCIÓN</p>
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/produccion-educacion-ejecutiva-icon.png" alt="Producción">
                    <p class="title_area">PRODUCCIÓN</p>
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/cadena-suministro-educacion-ejecutiva-icon.png" alt="Cadena de Suministro">
                    <p class="title_area">CADENA DE SUMINISTRO</p>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/medio-ambiente-educacion-ejecutiva-icon.png" alt="Medio Ambiente">
                    <p class="title_area">SEGURIDAD INDUSTRIAL Y MEDIO AMBIENTE</p>
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/sistemas-educacion-ejecutiva-icon.png" alt="Sistemas">
                    <p class="title_area">SISTEMAS</p>
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/area-legal-educacion-ejecutiva-icon.png" alt="área legal">
                    <p class="title_area">ÁREA LEGAL</p>
                </div>
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/gastronomia-turismo-educacion-ejecutiva-icon.png" alt="Turismo">
                    <p class="title_area">GASTRONOMÍA Y TURISMO</p>
                </div>
            </div>


            <div class="row">
                <div class="col-6 col-sm-3 col-md-3 col-lg-3 col-xl-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/idiomas-educacion-ejecutiva-icon.png" alt="Idiomas">
                    <p class="title_area">IDIOMAS</p>
                </div>
            </div>
        </div>
    </div>
    <div class="soluciones_corporativas_planes text-center">
        <div class="container">
            <p class="title">Contamos con planes de Educación Continua que se adaptan a todas tus necesidades empresariales</p>
            <br>
            <div class="w-100">
                <p class="subtitle_content_blue">Capacitación o Desarrollo</p>
                <div class="dumb_rectangle"></div>
                <div class="text-center row">
                    <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1 d-sm-none d-md-block p-0">
                        &nbsp;
                    </div>
                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 section_square">
                        <p class="subtitle_content_blue">Cursos-talleres</p>
                        <p class="content_little">Desarrollados de un solo tema.</p>
                    </div>
                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 section_square">
                        <p class="subtitle_content_blue">Seminarios</p>
                        <p class="content_little">Su desarrollo es de varios temas con uno o más expositores.</p>
                    </div>
                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 section_square">
                        <p class="subtitle_content_blue">Diplomados</p>
                        <p class="content_little">Es un programa curricular diseñado con temas especificos.</p>
                    </div>
                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 section_square">
                        <p class="subtitle_content_blue">Certificaciones</p>
                        <p class="content_little">Distinción profesional que cumple con estándares para desempeñar una labor en áreas determinadas.</p>
                    </div>
                    <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 section_square">
                        <p class="subtitle_content_blue">Idiomas</p>
                        <p class="content_little">Inglés, Francés, Alemán, Italiano, Portugues, Árabe.</p>
                    </div>
                    <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1 d-sm-none d-md-block p-0">
                        &nbsp;
                    </div>
                </div>
            </div>

            <!-- <div class="w-100">
                <br><br>
                <p class="subtitle_content_blue">Consultoría</p>
                <div class="dumb_rectangle"></div>
                <p class="subtitle_content_blue">Asesoría o desarrollo de:</p>
                <div class="text-center">
                    <p class="content_little">· DNC</p>
                    <p class="content_little">· Gestion por competencias</p>
                    <p class="content_little">· Diseño de programas de desarrollo y contenido</p>
                    <p class="content_little">· Universidad Corporativa</p>
                    <p class="content_little">· Coaching directivo</p>
                </div>
            </div> -->

        </div>
    </div>
    <!-- Page footer section -->
    <?php include_once('src/partial/footer.php'); ?>

</div>



<!-- Scripts -->
<?php include_once('src/partial/js.php'); ?>

</body>
</html>
