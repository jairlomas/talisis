<!DOCTYPE html>
<html lang="es" ng-app="MetodikaTI">
<head>
    <?php include_once('src/partial/head.php'); ?>
</head>

<body>

<!-- Preloader -->
<div class="preloader"></div>

<!-- Page header section -->
<?php include_once('src/partial/header.php'); ?>

<!-- All page content goes inside this div -->
<div id="pageContent" class="main-wrapper">


    <div class="container-fluid top_header_interior bg-nosotros text-center">
        <div class="container">
            <p class="title">TALISIS: LEARNING & DEVELOPMENT</p>
        </div>
    </div>


    <div class="container-fluid crecimiento_empresa text-center">
        <div class="container">
            <h2 class="title">Crecimiento y Desarrollo para tu Empresa</h2>
            <p class="content">En Talisis: Learning & Development estamos enfocados a ayudar a crecer tu empresa mediante el fortalecimiento de tu capital.</p>
            <br>
            <p class="subtitle">Brindamos servicios para impulsar el talento de tus colaboradores</p>
            <div class="row">
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 servicio_colaboradores">
                    <img src="assets/img/nosotros/capacitacion-ejecutiva-icon.png" class="img-fluid" alt="Capacitación Ejecutiva">
                    <p>CAPACITACIÓN EJECUTIVA</p>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 servicio_colaboradores">
                    <img src="assets/img/nosotros/capacitacion-ejecutiva-icon.png" class="img-fluid" alt="Educación Continua">
                    <p>EDUCACIÓN CONTINUA</p>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 servicio_colaboradores">
                    <img src="assets/img/nosotros/capacitacion-ejecutiva-icon.png" class="img-fluid" alt="Consultoría"> 
                    <p>CONSULTORÍA</p>
                </div>
            </div>
        </div>
    </div>



    <div class="container-fluid blue_slogan text-left">
        <div class="container">
            <h2 class="content">DEDICADOS A DISEÑAR SOLUCIONES A LA MEDIDA</h2>
        </div>
    </div>


    <div class="container-fluid servicios_metodologia text-center">
        <div class="container">
            <h2 class="content">Nuestro servicios cuentan con una metodología que permite garantizar la calidad de los programas desarrollados para tu empresa</h2>
        </div>
    </div>



    <div class="container-fluid info_types text-center">
        <div class="row">
            <div class="col-md-6 bg-info-1-smarthphone">
                <h2>GRUPOS A LA MEDIDA</h2>
            </div>
            <div class="col-md-6 bg-info-1"></div>
        </div>
        <div class="row">
            <div class="col-md-6 bg-info-2"></div>
            <div class="col-md-6 bg-info-2-smarthphone">
                <h2>ENFOQUE PRÁCTICO</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 bg-info-3-smarthphone">
                <h2>DESARROLLO DE PROYECTOS</h2>
            </div>
            <div class="col-md-6 bg-info-3"></div>
        </div>
        <div class="row">
            <div class="col-md-6 bg-info-4"></div>
            <div class="col-md-6 bg-info-4-smarthphone">
                <h2>HORARIOS FLEXIBLES</h2>
            </div>
        </div>
    </div>



    <div class="container-fluid soluciones_capacitacion text-center">
        <div class="container">
            <h2 class="title">Creamos soluciones de capacitación, educación o consultoría de acuerdo a tu empresa</h2>
            <br><br>
            <div class="row">
                <div class="col p-0">
                    <img src="assets/img/nosotros/paso1-deteccion-de-necesidades-gpc.jpg" class="img-fluid" alt="Detección">
                </div>
                <div class="col p-0">
                    <img src="assets/img/nosotros/paso2-diseno-de-soluciones-gpc.jpg" class="img-fluid" alt="Diseño">
                </div>
                <div class="col p-0">
                    <img src="assets/img/nosotros/paso3-propuesta-gpc.jpg" class="img-fluid" alt="Propuesta">
                </div>
                <div class="col p-0">
                    <img src="assets/img/nosotros/paso4-implementacion-gpc.jpg" class="img-fluid" alt="Implementación">
                </div>
                <div class="col p-0">
                    <img src="assets/img/nosotros/paso5-evaluacion-gpc.jpg" class="img-fluid" alt="Evaluación">
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid socios_estrategicos text-center">
        <div class="">
            <h2 class="title">Talisis: Learning & Development cuenta con varios socios estratégicos que le permiten entregar un servicio de alta calidad a las empresas y sus futuros estudiantes.</h2>
            <br>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1 d-sm-none d-md-block">&nbsp;</div>
                <div class="col-sm-6 col-md-2 col-lg-2 col-xl-2 content_section_proyect">
                    <div class="bg-image-1"></div>
                    <div class="text-left">
                        <p class="title_content">Kinedu</p>
                        <p class="content">Kinedu impulsa el desarrollo de la primera infancia por medio de productos, actividades de estimulación y una aplicación móvil que fomenta mejores interacciones entre adultos y niños. Además, promueve el desarrollo físico y mental de los bebés a través del análisis de las experiencias entre padres e hijos.</p>
                        <a class="link_site" href="http://www.kinedu.com" target="_blank">www.kinedu.com</a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-2 col-lg-2 col-xl-2 content_section_proyect">
                    <div class="bg-image-2"></div>
                    <div class="text-left">
                        <p class="title_content">Advenio</p>
                        <p class="content">Tiene el objetivo de impulsar la primera infancia con la ayuda de los adultos que constantemente interactúan con los niños. Advenio es un espacio de aprendizaje integral que busca incentivar el crecimiento familiar y desarrollo infantil de la mano de los padres.</p>
                        <a class="link_site" href="http://www.advenio.mx" target="_blank">www.advenio.mx</a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-2 col-lg-2 col-xl-2 content_section_proyect">
                    <div class="bg-image-3"></div>
                    <div class="text-left">
                        <p class="title_content">U-ERRE</p>
                        <p class="content">Institución de educación superior con la finalidad de formar profesionales capaces e íntegros bajo un sistema educativo de alta calidad, por medio de una oferta académica amplia y especializada en distintas áreas de enseñanza.</p>
                        <a class="link_site" href="http://www.u-erre.mx" target="_blank">www.u-erre.mx</a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-2 col-lg-2 col-xl-2 content_section_proyect">
                    <div class="bg-image-4"></div>
                    <div class="text-left">
                        <p class="title_content">UNID</p>
                        <p class="content">La Universidad Interamericana para el Desarrollo ofrece programas de licenciatura y posgrado con la finalidad de preparar alumnos íntegros con un buen desempeño personal y profesional.</p>
                        <a class="link_site" href="http://www.unid.edu.mx" target="_blank">www.unid.edu.mx</a>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-2 col-lg-2 col-xl-2 content_section_proyect">
                    <div class="bg-image-5"></div>
                    <div class="text-left">
                        <p class="title_content">IESALUD</p>
                        <p class="content">El Instituto de Educación Superior en Estudios de la Salud cuenta con programas de bachillerato técnico y licenciaturas en enfermería como parte de la comunidad U-ERRE y tiene el objetivo de ofrecer programas de educación superior especializados en ciencias de la salud.</p>
                        <a class="link_site" href="http://www.iesalud.edu.mx" target="_blank">www.iesalud.edu.mx</a>
                    </div>
                </div>
                <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1 d-sm-none d-md-block">&nbsp;</div>
            </div>
        </div>
    </div>
    <!-- Page footer section -->
    <?php include_once('src/partial/footer.php'); ?>

</div>



<!-- Scripts -->
<?php include_once('src/partial/js.php'); ?>

</body>
</html>
