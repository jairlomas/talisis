<!DOCTYPE html>
<html lang="es" ng-app="MetodikaTI">
<head>
    <?php include_once('src/partial/head.php'); ?>
</head>

<body>

    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Page header section -->
    <?php include_once('src/partial/header.php'); ?>

    <!-- All page content goes inside this div -->
    <div id="pageContent" class="main-wrapper">

        <div class="slider_top">
            <div class="slider_1">
                <section class="section-top-video">
                    <div class="container text-center ">
                        <div class="row">
                            <div class="col">
                                <div class="text-center">
                                    <img src="assets/img/home/lnd-id.svg" class="nav_icon" alt="icono">
                                </div>
                            </div>
                        </div>
                        <!-- <p class="title">TALISIS</p> -->
                        <!-- <p class="subtitle">LEARNING AND DEVELOPMENT</p>
                        <p class="subtitle">DRIVE CHANGE TO EMPOWER YOUR BUSINESS</p> -->
                        <!-- <p class="little_subtitle">A TALISIS PROJECT</p> -->
                    </div>
                </section>
            </div>
            <div class="slider_2">
                <section class="section-top-video">
                    <div class="container text-center ">
                        <div class="row">
                            <div class="col">
                                <div class="text-center">
                                    <img src="assets/img/home/lnd-id.svg" class="nav_icon" alt="icono">
                                </div>
                            </div>
                        </div>
                        <!-- <p class="title">TALISIS</p> -->
                        <!-- <p class="subtitle">LEARNING AND DEVELOPMENT</p>
                        <p class="subtitle">DRIVE CHANGE TO EMPOWER YOUR BUSINESS</p> -->
                        <!-- <p class="little_subtitle">A TALISIS PROJECT</p> -->
                    </div>
                </section>
            </div>
            <div class="slider_3">
                <section class="section-top-video">
                    <div class="container text-center ">
                        <div class="row">
                            <div class="col">
                                <div class="text-center">
                                    <img src="assets/img/home/lnd-id.svg" class="nav_icon" alt="icono">
                                </div>
                            </div>
                        </div>
                        <!-- <p class="title">TALISIS</p> -->
                        <!-- <p class="subtitle">LEARNING AND DEVELOPMENT</p>
                        <p class="subtitle">DRIVE CHANGE TO EMPOWER YOUR BUSINESS</p> -->
                        <!-- <p class="little_subtitle">A TALISIS PROJECT</p> -->
                    </div>
                </section>
            </div>
            <div class="slider_4">
                <section class="section-top-video">
                    <div class="container text-center ">
                        <div class="row">
                            <div class="col">
                                <div class="text-center">
                                    <img src="assets/img/home/lnd-id.svg" class="nav_icon" alt="icono">
                                </div>
                            </div>
                        </div>
                        <!-- <p class="title">TALISIS</p> -->
                        <!-- <p class="subtitle">LEARNING AND DEVELOPMENT</p>
                        <p class="subtitle">DRIVE CHANGE TO EMPOWER YOUR BUSINESS</p> -->
                        <!-- <p class="little_subtitle">A TALISIS PROJECT</p> -->
                    </div>
                </section>
            </div>
        </div>






        <section class="container-fluid que_es_talisis" id="que_es_talisis">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 content_section">
                    <div class="">
                        <div class="col-sm-12 col-md-10 col-lg-10 col-xl-10 offset-md-1 offset-lg-1 offset-xl-1">
                            <p class="title">¿Qué es Talisis: Learning & Development ?</p>
                            <p class="description">Ayudamos a enriquecer el talento por medio de experiencias de alta calidad de capacitación donde se desarrollan las habilidades y competencias del ser.</p>
                            <p class="description">Cada modelo de capacitación se crea de acuerdo a las necesidades específicas de tu empresa.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-6 col-xl-6 d-flex align-items-center text-center play_video pd0">
                    <!-- <img src="assets/img/play_button.png" class="play_button  js-modal-btn" data-video-id="tHhRWBw53J8" alt="play_video"> -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tHhRWBw53J8?ecver=1" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>


        <section class="container-fluid section_types">
            <div class="row">

                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 bg-1 p-0">
                    <div class="section_off">
                        <p class="title_off">Capacitación Ejecutiva</p>
                    </div>
                    <div class="section_on align-middle">
                        <p class="title_on">Capacitación Ejecutiva</p>
                        <p class="description">Desarrollamos el modelo de capacitación que tu empresa necesita para lograr sus objetivos.<br><br>Nuestro amplio conocimiento y experiencia en capacitación ejecutiva nos dan el respaldo y la confianza para ofrecerte los mejores modelos de capacitación.</p>
                        <a href="educacion-ejecutiva.php" class="see_more">Ver mas [ + ]</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 bg-2 p-0">
                    <div class="section_off">
                        <p class="title_off">Aprendizaje y Desarrollo</p>
                    </div>
                    <div class="section_on">
                        <p class="title_on">Aprendizaje y Desarrollo</p>
                        <p class="description">Contamos con los mejores planes de Aprendizaje y Desarrollo que necesitas para fortalecer el talento de tu empresa.</p>
                        <a href="educacion-continua.php" class="see_more">Ver mas [ + ]</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 bg-3 p-0">
                    <div class="section_off">
                        <p class="title_off">Consultoría</p>
                    </div>
                    <div class="section_on">
                        <p class="title_on">Consultoría</p>
                        <p class="description">Diseñamos soluciones a partir de un diagnóstico de oportunidades de mejora para que tu organización cumpla sus objetivos.</p>
                        <a href="talisis-consultoria.php" class="see_more">Ver mas [ + ]</a>
                    </div>
                </div>

            </div>
        </section>
        <section class="container-fluid empresas_confian_nosotros">
            <div class="container text-center">
                <h1><p class="title">Alianzas estratégicas:</p></h1>
                <div class="empresas">
                    <div class="align-middle">
                        <img src="assets/img/home/clientes/arca-id.jpg" alt="arca" onclick="window.open('http://www.arcacontal.com/', '_blank');">
                    </div>
                    <div><img src="assets/img/home/clientes/fep-id.jpg" alt="fep-id" onclick="window.open('http://fepnl.org.mx/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/yeseramty-id.jpg" alt="yeseramty" onclick="window.open('http://www.yeseramonterrey.com/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/grupofamsa-id.jpg" alt="Famsa" onclick="window.open('http://www.grupofamsa.com/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/imss-id.jpg" alt="imss" onclick="window.open('http://www.imss.gob.mx/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/teleperformance-id.jpg" alt="teleperformance" onclick="window.open('https://www.teleperformance.com', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/cemex-id.jpg" alt="cemex" onclick="window.open('https://www.cemex.com/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/lambi-id.jpg" alt="lambi" onclick="window.open('http://www.lambi.com.mx/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/zinc-id.jpg" alt="zinc" onclick="window.open('http://www.zincnacional.com/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/infosys-id.jpg" alt="infosys" onclick="window.open('https://www.infosys.com/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/axa-id.jpg" alt="axa" onclick="window.open('https://www.axa.com/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/coflex-id.jpg" alt="coflex" onclick="window.open('http://www.coflex.com.mx/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/monterreygob-id.jpg" alt="monterrey" onclick="window.open('http://www.monterrey.gob.mx/oficial/index-Portal.asp', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/cataforesis-id.jpg" alt="cataforesis" onclick="window.open('http://www.cataforesis.info', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/enerya-id.jpg" alt="enerya"></div>
                    <div><img src="assets/img/home/clientes/chedraui-id.jpg" alt="chedraui" onclick="window.open('https://www.chedraui.com.mx/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/delphi-id.jpg" alt="delphi" onclick="window.open('https://www.delphi.com/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/multimedios-id.jpg" alt="multimedios" onclick="window.open('http://www.multimedios.com/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/vitro-id.jpg" alt="vitro" onclick="window.open('http://www.vitro.com/', '_blank');"></div>
                    <div><img src="assets/img/home/clientes/benavides-id.jpg" alt="benavides" onclick="window.open('http://www.benavides.com.mx/', '_blank');"></div>
                </div>
            </div>
        </section>
        <section class="container-fluid alcance_proyectos bg-gray">
            <div class="container">
                <p class="title text-center">CONOCE EL ALCANCE DE NUESTROS PROYECTOS</p>
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-xl-12 col-lg-12">
                        <div class="row">
                            <div class="col text-center section_circle">
                                <div class="total_proyectos">+450</div>
                                <p class="content_proyecto">Profesores y Coaches</p>
                            </div>
                            <div class="col text-center section_circle">
                                <div class="total_proyectos">+2000</div>
                                <p class="content_proyecto">Colaboradores en Transformación</p>
                            </div>
                            <div class="col text-center section_circle">
                                <div class="total_proyectos">+30</div>
                                <p class="content_proyecto">Alianzas</p>
                            </div>
                            <div class="col text-center section_circle">
                                <div class="total_proyectos">+60</div>
                                <p class="content_proyecto">Centros de Aprendizaje y Desarrollo</p>
                            </div>
                            <div class="col text-center section_circle">
                                <div class="total_proyectos">+150</div>
                                <p class="content_proyecto">Soluciones Diseñadas</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div id="tooltip_state">
            <p>Nuevo Leon</p>
        </div>
        <section class="container-fluid alcance_proyectos mapa_proyectos">
            <div class="container">
                <p class="title text-center">OPERACIÓN DE PROYECTOS EN AMÉRICA</p>
                <div class="row">
                        <div class="col-sm-10 col-md-10 col-xl-10 col-lg-10 offset-sm-1 offset-md-1 offset-xl-1 offset-lg-1 text-center">
                        <img src="assets/img/home/mapa/mapas_nuevo/default.png" usemap="#Map" class="img-fluid mapa_mexico" alt="Mapa" />
                        <map name="Map" id="Map">
                            <area estado="dallas" class="estado" shape="poly" coords="259,261,261,262,264,263,267,261,268,258,266,255,262,255,259,257" />
                            <area estado="chihuahua" class="estado" shape="poly" coords="196,292,198,293,202,293,203,291,202,287,200,285,197,286,195,289" />
                            <area estado="nuevo_leon" class="estado" shape="poly" coords="264,331,266,331,269,330,269,326,266,323,262,323,261,326,261,329" />
                            <area estado="durango" class="estado" shape="poly" coords="210,341,213,340,215,337,214,333,211,332,207,333,206,336,207,339" />
                            <area estado="aguascalientes" class="estado" shape="poly" coords="236,369,238,370,240,369,242,367,241,364,240,362,236,361,234,362,233,365,233,367" />
                            <area estado="jalisco" class="estado" shape="poly" coords="222,389,224,389,226,388,227,385,226,381,223,380,220,380,218,382,218,385,218,387,220,389" />
                            <area estado="colima" class="estado" shape="poly" coords="221,393,219,393,217,395,216,399,217,401,219,402,222,402,224,402,226,399,226,396,224,394" />
                            <area estado="queretaro" class="estado" shape="poly" coords="264,383,266,383,268,382,269,380,270,377,267,375,264,374,261,375,260,378,260,381,262,382" />
                            <area estado="cdmx" class="estado" shape="poly" coords="265,392,264,394,264,397,265,399,269,400,272,399,273,396,273,394,271,392,268,391" />
                            <area estado="edomex" class="estado" shape="poly" coords="262,393,260,391,256,390,253,390,252,392,252,394,252,396,254,397,256,398,258,399,260,398,261,396" />
                            <area estado="guerrero" class="estado" shape="poly" coords="262,410,260,410,258,413,258,415,259,417,261,419,265,419,267,416,267,413,266,410,264,409" />
                            <area estado="chiapas" class="estado" shape="poly" coords="345,423,342,423,341,425,341,429,343,431,346,432,349,430,350,427,348,424" />
                            <area estado="campeche" class="estado" shape="poly" coords="369,391,366,392,364,394,365,399,368,401,371,400,373,398,373,394,371,392" />
                            <area estado="quintanaroo" class="estado" shape="poly" coords="390,386,388,388,387,391,387,393,389,395,391,396,393,395,395,393,395,390,394,388,392,386" />
                            <area estado="ecuador" class="estado" shape="poly" coords="488,600,486,601,484,603,484,605,485,609,487,610,490,610,493,608,493,605,492,602,490,600" />
                            <area estado="peru" class="estado" shape="poly" coords="511,729,509,730,507,732,507,735,508,738,511,739,515,738,517,735,517,733,515,730,513,729" />
                            <area estado="argentina" class="estado" shape="poly" coords="639,878,636,879,635,882,635,886,638,887,641,888,643,885,644,882,642,879" />
                        </map>
                    </div>
                </div>
            </div>
        </section>
        <?php include_once('src/partial/footer.php'); ?>
    </div>
    <?php include_once('src/partial/js.php'); ?>

</body>
</html>