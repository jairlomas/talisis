<!DOCTYPE html>
<html lang="es" ng-app="MetodikaTI">
<head>
    <?php include_once('src/partial/head.php'); ?>
</head>

<body>

<!-- Preloader -->
<div class="preloader"></div>

<!-- Page header section -->
<?php include_once('src/partial/header.php'); ?>


<!-- All page content goes inside this div -->
<div id="pageContent" class="main-wrapper">

    <div class="container-fluid top_header_interior bg-educacion_ejecutiva text-center">
        <div class="container">
            <p class="title">CAPACITACIÓN QUE HACE<br><big-letter>CRECER</big-letter> TU EMPRESA</p>
        </div>
    </div>




    <div class="licenciatura container-fluid text-center">
        <div class="container">
            <p class="title">También brindamos Preparatorias y Licenciaturas.<br>Te mencionamos algunos de nuestros clientes:</p>
            <br>
            <div class="row">
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 text-center">
                    <img class="logo_bussiness" src="assets/img/home/cemex-gris-educacion-continua-img.png" alt="colaboradores">
                    <br>
                    <p class="subtitle_content">Prepa Empresa:</p>
                    <p class="content_little">212 colaboradores egresados</p>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 text-center">
                    <img class="logo_bussiness" src="assets/img/home/chedraui-gris-educacion-continua-img.png" alt="educacion-continua">
                    <br>
                    <p class="subtitle_content">Licenciatura Corporativa:</p>
                    <p class="content_little">103 colaboradores inscritos</p>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4 text-center">
                    <img class="logo_bussiness" src="assets/img/home/grupocomercialcontrol-gris-educacion-continua-img.png" alt="Grupo">
                    <br>
                    <p class="subtitle_content">Licenciatura Corporativa:</p>
                    <p class="content_little">234 colaboradores egresados</p>
                    <p class="content_little">600 colaboradores inscritos</p>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid conoce_planes text-center">
        <div class="container">
            <p class="subtitle_content">Los planes que ofrecemos son:</p>
            <div class="text-center min-height-150 row">
                <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1 d-sm-none d-md-block">&nbsp;</div>
                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 b-right-blue">
                    <p class="subtitle_content_blue">Prepa Empresa</p>
                    <p class="content_little">2 años.</p>
                </div>
                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 b-right-blue">
                    <p class="subtitle_content_blue">Licenciatura</p>
                    <p class="content_little">3 años.</p>
                </div>
                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 b-right-blue">
                    <p class="subtitle_content_blue">Ingeniería</p>
                    <p class="content_little">3 años 4 meses.</p>
                </div>
                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 b-right-blue">
                    <p class="subtitle_content_blue">Posgrado</p>
                    <p class="content_little">2 años 4 meses.</p>
                </div>
                <div class="col-sm-12 col-md-2 col-lg-2 col-xl-2 b-right-blue">
                    <p class="subtitle_content_blue">Especialidades</p>
                    <p class="content_little">1 a 1.5 años según la especialidad.</p>
                </div>
                <div class="col-sm-1 col-md-1 col-lg-1 col-xl-1 d-sm-none d-md-block">&nbsp;</div>
            </div>
        </div>
    </div>


    <!-- Page footer section -->
    <?php include_once('src/partial/footer.php'); ?>

</div>
<!-- Scripts -->
<?php include_once('src/partial/js.php'); ?>

</body>
</html>
