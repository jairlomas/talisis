<?php

$nombre = $_POST['nombre_completo'];
$email = $_POST['email'];
$telefono = $_POST['telefono'];
$empresa = $_POST['empresa'];
$estado = $_POST['estado'];
$comentario = $_POST['comentario'];

$email = "";
$responsable = "";

switch ($estado) {
    case "Aguascalientes":      $email = "ecastaneda@unid.mx";              $responsable = "Enrique Castañeda"; break;
    case "Baja California":     $email = "alvaro.garcia@talisis.com";       $responsable = "Alvaro Garcia"; break;
    case "Baja California Sur": $email = "alvaro.garcia@talisis.com";       $responsable = "Alvaro Garcia"; break;
    case "Campeche":            $email = "ernesto.garza@talisis.com";       $responsable = "Ernesto Garza"; break;
    case "Chiapas":             $email = "ernesto.garza@talisis.com";       $responsable = "Ernesto Garza"; break;
    case "Chihuahua":           $email = "alvaro.garcia@talisis.com";       $responsable = "Alvaro Garcia"; break;
    case "Ciudad de México":    $email = "federico.fernandez@talisis.com";  $responsable = "Federico Fernandez"; break;
    case "Coahuila":            $email = "alvaro.garcia@talisis.com";       $responsable = "Alvaro Garcia"; break;
    case "Colima":              $email = "rocio.nuno@talisis.com";          $responsable = "Rocio Nuño"; break;
    case "Durango":             $email = "alvaro.garcia@talisis.com";       $responsable = "Alvaro Garcia"; break;
    case "Guanajuato":          $email = "ecastaneda@unid.mx";              $responsable = "Enrique Castañeda"; break;
    case "Guerrero":            $email = "federico.fernandez@talisis.com";  $responsable = "Federico Fernandez"; break;
    case "Hidalgo":             $email = "federico.fernandez@talisis.com";  $responsable = "Federico Fernandez"; break;
    case "Jalisco":             $email = "rocio.nuno@talisis.com";          $responsable = "Rocio Nuño"; break;
    case "México":              $email = "federico.fernandez@talisis.com";  $responsable = "Federico Fernandez"; break;
    case "Michoacán":           $email = "rocio.nuno@talisis.com";          $responsable = "Rocio Nuño"; break;
    case "Morelos":             $email = "federico.fernandez@talisis.com";  $responsable = "Federico Fernandez"; break;
    case "Nayarit":             $email = "alvaro.garcia@talisis.com";       $responsable = "Alvaro Garcia"; break;
    case "Nuevo León":          $email = "alvaro.garcia@talisis.com";       $responsable = "Alvaro Garcia"; break;
    case "Oaxaca":              $email = "ernesto.garza@talisis.com";       $responsable = "Ernesto Garza"; break;
    case "Puebla":              $email = "federico.fernandez@talisis.com";  $responsable = "Federico Fernandez"; break;
    case "Querétaro":           $email = "ecastaneda@unid.mx";              $responsable = "Enrique Castañeda"; break;
    case "Quintana Roo":        $email = "ernesto.garza@talisis.com";       $responsable = "Ernesto Garza"; break;
    case "San Luis Potosí":     $email = "ecastaneda@unid.mx";              $responsable = "Enrique Castañeda"; break;
    case "Sinaloa":             $email = "alvaro.garcia@talisis.com";       $responsable = "Alvaro Garcia"; break;
    case "Sonora":              $email = "alvaro.garcia@talisis.com";       $responsable = "Alvaro Garcia"; break;
    case "Tabasco":             $email = "ernesto.garza@talisis.com";       $responsable = "Ernesto Garza"; break;
    case "Tamaulipas":          $email = "alvaro.garcia@talisis.com";       $responsable = "Alvaro Garcia"; break;
    case "Tlaxcala":            $email = "federico.fernandez@talisis.com";  $responsable = "Federico Fernandez"; break;
    case "Veracruz":            $email = "ernesto.garza@talisis.com";       $responsable = "Ernesto Garza"; break;
    case "Yucatán":             $email = "ernesto.garza@talisis.com";       $responsable = "Ernesto Garza"; break;
    case "Zacatecas":           $email = "ecastaneda@unid.mx";              $responsable = "Enrique Castañeda"; break;

}



$headers = 'From: TALISIS <contacto@futurite.com>' . "\r\n".
			'Reply-To: '.$_POST["email"]. "\r\n" .
			"MIME-Version: 1.0\r\n".
			"Content-Type: text/html; charset=UTF-8\r\n".
			'X-Mailer: PHP/' . phpversion();
 


//$email_to = $email;
$email_to = "jair.lomas@metodika.mx";

$asunto ="TALISIS";

$mensaje = "<b>TALISIS:</b> .<br>";
$mensaje .= "Nombre: " . $nombre . ".<br>";
$mensaje .= "Correo Electronico: " . $email . ".<br>";
$mensaje .= "Numero de Telefono: " . $telefono . ".<br>";
$mensaje .= "Nombre de la Empresa: " . $empresa . ".<br>";
$mensaje .= "Estado: " . $estado . ".<br>";
$mensaje .= "Comentario: " . $comentario . ".<br>";

if(@mail($email_to, $asunto, $mensaje, $headers)) {

    $response["error"] = false;
    $response["message"] = "Tus datos de contacto se han enviado correctamente, pronto nos pondremos en contacto contigo.";
    echo json_encode($response);

} else{

    $response["error"] = true;
    $response["message"] = "Tus datos de contacto no se han podido enviar, inténtelo nuevamente más tarde o comunícate a nuestro teléfono + 52 123-456-7890.";
    echo json_encode($response);

}
?>