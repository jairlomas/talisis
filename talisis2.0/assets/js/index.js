$(document).ready(function () {

    setTimeout(function(){ 
        $('.five9-frame').find('.five9-text').text('Atención en Línea');
    }, 100);

    $("map").imageMapResize();

    var WINDOW_WIDTH = $(window).width();
    var WINDOW_HEIGHT = $(window).height();

    if(WINDOW_WIDTH <= 767 && WINDOW_WIDTH > 375) {
        var slider_width = $(".empresas_confian_nosotros .container").width() / 2;
        $('.empresas').bxSlider({
            minSlides: 1,
            maxSlides: 2,
            slideWidth: slider_width,
            slideMargin: 0,
            auto: true,
            pager: false,
        });
    }else if(WINDOW_WIDTH <= 375) {
        var slider_width = $(".empresas_confian_nosotros .container").width();
        $('.empresas').bxSlider({
            minSlides: 1,
            maxSlides: 1,
            slideWidth: slider_width,
            slideMargin: 0,
            auto: true,
            pager: false,
        });
    }else{
        var slider_width = $(".empresas_confian_nosotros .container").width() / 3;
        $('.empresas').bxSlider({
            minSlides: 1,
            maxSlides: 3,
            slideWidth: slider_width,
            slideMargin: 0,
            auto: true,
        });
    }

    var slider_width = $(window).width();
    $(function(){
        $('.slider_top').bxSlider({
            mode: 'fade',
            captions: false,
            pager: false,
            slideWidth: slider_width,
            auto: true,
            pause: 3000,
            controls: false,
        });
    });


    var nav_bar_height = $("nav").outerHeight();
    $("#pageContent").css("padding-top", nav_bar_height);


    $(".js-modal-btn").modalVideo();


    $(this).on('submit', '#form_contacto', function (e) {
        e.preventDefault();

        $.ajax({
            type       : 'POST',
            data       : $(this).serialize(),
            dataType   : 'json',
            url        : $(this).attr('action'),
            beforeSend : function () {
                blockForm();
            },
            error      : function (jqXHR) {
                unblockForm();

                bootbox.alert(response.message, function () {
                    window.location = "Tus datos de contacto no se han enviado, inténtelo nuevamente más tarde o comunícate a nuestro teléfono 52 123-456-7890.";
                });

            },
            success    : function (response) {
                unblockForm();

                if (response.error == true) {
                    bootbox.alert(response.message, function () {
                        //window.location = response.route;
                    });
                } else {
                    bootbox.alert(response.message, function () {
                        //window.location = response.message;
                    });
                }
            }
        });

    });


    $(".estado").hover(
        function () {
            var estado = $(this).attr("estado");

            console.log(estado);

            var imagen = "";
            switch (estado) {
                case "aguascalientes" :     imagen = "assets/img/home/mapa/mapas_nuevo/aguascalientes.png"; break;
                case "argentina" :          imagen = "assets/img/home/mapa/mapas_nuevo/argentina.png"; break;
                case "campeche" :           imagen = "assets/img/home/mapa/mapas_nuevo/campeche.png"; break;
                case "cdmx" :               imagen = "assets/img/home/mapa/mapas_nuevo/cdmx.png"; break;
                case "chiapas" :            imagen = "assets/img/home/mapa/mapas_nuevo/chiapas.png"; break;
                case "chihuahua" :          imagen = "assets/img/home/mapa/mapas_nuevo/chihuahua.png"; break;
                case "colima" :             imagen = "assets/img/home/mapa/mapas_nuevo/colima.png"; break;
                case "dallas" :             imagen = "assets/img/home/mapa/mapas_nuevo/dallas.png"; break;
                case "durango" :            imagen = "assets/img/home/mapa/mapas_nuevo/durango.png"; break;
                case "ecuador" :            imagen = "assets/img/home/mapa/mapas_nuevo/ecuador.png"; break;
                case "edomex" :             imagen = "assets/img/home/mapa/mapas_nuevo/edomex.png"; break;
                case "guerrero" :           imagen = "assets/img/home/mapa/mapas_nuevo/guerrero.png"; break;
                case "jalisco" :            imagen = "assets/img/home/mapa/mapas_nuevo/jalisco.png"; break;
                case "nuevo_leon" :         imagen = "assets/img/home/mapa/mapas_nuevo/nuevoleon.png"; break;
                case "peru" :               imagen = "assets/img/home/mapa/mapas_nuevo/peru.png"; break;
                case "queretaro" :          imagen = "assets/img/home/mapa/mapas_nuevo/queretaro.png"; break;
                case "quintanaroo" :        imagen = "assets/img/home/mapa/mapas_nuevo/quintanaroo.png"; break;
            }

            var contenido = "";
            switch (estado) {
                case "aguascalientes" :     contenido = "Aguascalientes, México.<br>Empresa: <br><strong>&middot;Arca Continental.</strong><br><strong>&middot;Grupo Comercial Control.</strong><br><strong>&middot;Grupo Gentera.</strong>"; break;
                case "argentina" :          contenido = "Argentina, México.<br>Empresa: <br><strong>&middot;Arca Continental.</strong>"; break;
                case "campeche" :           contenido = "Campeche, México.<br>Empresa: <br><strong>&middot;Grupo Gentera</strong>"; break;
                case "cdmx" :               contenido = "Ciudad de México, México.<br>Empresa: <br><strong>&middot;Grupo Comercial Control.</strong><br><strong>&middot;Grupo Gentera.</strong>"; break;
                case "chiapas" :            contenido = "Chiapas, México.<br>Empresa: <br><strong>&middot;Grupo Gentera.</strong>"; break;
                case "chihuahua" :          contenido = "Chihuahua, México.<br>Empresa: <br><strong>&middot;Grupo Comercial Control.</strong><br><strong>&middot;Grupo Gentera.</strong><br><strong>&middot;Arca Continental.</strong>"; break;
                case "colima" :             contenido = "Colima, México.<br>Empresa: <br><strong>&middot;Grupo Gentera</strong>"; break;
                case "dallas" :             contenido = "Monterrey, México.<br>Empresa: <br><strong>&middot;Arca Continental.</strong>"; break;
                case "durango" :            contenido = "Durango, México.<br>Empresa: <br><strong>&middot;Grupo Comercial Control.</strong>"; break;
                case "ecuador" :            contenido = "Monterrey, México.<br>Empresa: <br><strong>&middot;Arca Continental</strong>"; break;
                case "edomex" :             contenido = "Estado de México, México.<br>Empresa: <br><strong>&middot;Grupo Gentera</strong>"; break;
                case "guerrero" :           contenido = "Guerrero, México.<br>Empresa: <br><strong>&middot;Grupo Gentera</strong>"; break;
                case "jalisco" :            contenido = "Guadalajara, México.<br>Empresa: <br><strong>&middot;Grupo Gentera</strong><br><strong>&middot;Arca Continental</strong>"; break;
                case "nuevo_leon" :         contenido = "Monterrey, México.<br>Empresa: <br><strong>&middot;Grupo Comercial Control.</strong><br><strong>&middot;Arca Continental.</strong>"; break;
                case "peru" :               contenido = "Lima Perú.<br>Empresa: <br><strong>&middot;Arca Continental.</strong>"; break;
                case "queretaro" :          contenido = "Monterrey, México.<br>Empresa: <br><strong>&middot;Grupo Comercial Control.</strong><br><strong>&middot;Grupo Gentera.</strong>"; break;
                case "quintanaroo" :        contenido = "Quintana Roo, México.<br>Empresa: <br><strong>&middot;Grupo Gentera</strong>"; break;
            }

            if(imagen != "") {
                $("#tooltip_state p").html(contenido);
                $(".mapa_mexico").attr("src", imagen);
                $("#tooltip_state").css("display", "block");
            }

        }, function () {

            $(".mapa_mexico").attr("src", "assets/img/home/mapa/mapas_nuevo/default.png");
            $("#tooltip_state").css("display", "none");

     });


    var tooltipSpan = document.getElementById('tooltip_state');
    window.onmousemove = function (e) {
        var x = e.clientX,
            y = e.clientY;
        tooltipSpan.style.top = (y + 15) + 'px';
        tooltipSpan.style.left = (x + 15) + 'px';
    };


    $(".full_height").css("height", WINDOW_HEIGHT);

});



function blockForm() {
    formActions(true);
}

function unblockForm() {
    formActions(false);
}

function formActions(block) {
    $('input, select, button, a, textarea, checkbox').each(function () {
        $(this).attr('disabled', block);
    });
}