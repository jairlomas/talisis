
Five9SocialWidget.addWidget({
    type: "chat",
    lang: "https://metodika.com.mx/dsr/talisis/assets/js/chat-lang.js",
    rootUrl: "https://app.five9.com/consoles/",
    tenant: "Grupo Topaz Soluciones Educativas",
    title: "Learning & Development",
    profiles: ["Medios Digitales LD"],
    showProfiles: true,
    theme: "https://metodika.com.mx/dsr/talisis/assets/css/chat-theme.css",
    logo: "https://metodika.com.mx/dsr/talisis/assets/img/home/lnd-id.svg"
});