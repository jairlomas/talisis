<!DOCTYPE html>
<html lang="es" ng-app="MetodikaTI">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- SEO Metatags -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Metodikat TI">

    <!-- Structured data -->
    <?php include_once('src/partial/seo/structured-data.php'); ?>

    <!-- Googlebot -->
    <?php include_once('src/partial/seo/googlebot.php'); ?>

    <!-- Facebook Pixel Code -->
    <?php include_once('src/partial/seo/fb-pixel.php'); ?>

    <!-- Title -->
    <title>
        Talisis: Learning & Development - Nosotros
    </title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <!-- Preloader -->
    <link href="assets/css/preloader.css" rel="stylesheet" />
    <script src="assets/js/preloader.js"></script>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- CSS -->
    <link href="assets/css/main.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/bxslider/jquery.bxslider.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- AngularJS -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js"></script>
    <script src="app/app.js"></script>

    <!-- Analytics code -->
    <?php include_once('src/partial/seo/analytics.php'); ?>
</head>

<body>
<!-- Preloader -->
<div class="preloader"></div>

<!-- All page content goes inside this div -->
<div id="pageContent" class="main-wrapper">

    <!-- Page header section -->
    <?php include_once('src/partial/header2.php'); ?>

    <!-- Page content section -->
    <div class="top_header bg_educacion_continua">
        <div class="container text-center">
            <p class="titulo_interior">TALISIS: LEARNING & DEVELOPMENT</p>
        </div>
    </div>


    <div class="sec2Nosotros">
    	<div class="container">
    		<div class="parrafoNosotros">
                <div class="col-sm-10 col-sm-offset-1">
                    <span class="parrafoNosotros1"><p>Crecimiento y Desarrollo para tu Empresa</p> </span> 
                </div>
    			<div class="col-sm-9 col-sm-offset-2">
	    			<span class="parrafoNos">
		    			<p>
		    				En Talisis: Learning & Development estamos enfocados a ayudar a crecer tu empresa mediante el fortalecimiento de tu capital.
		    			</p>
	    			</span>	
    			</div>
    		</div>
    	</div>
    </div>

    <div class="container">
        <div class="col-sm-8 col-sm-offset-2 parrafoNosotros1-1">
            <p>Brindamos servicios para impulsar el talento de tus colaboradores</p>
        </div>
        <div class="col-sm-12 icnos3">
            <div class="col-sm-4 thumbnail">
                <img src="assets/img/nosotros/capacitacion-ejecutiva-icon.png">
                <div class="textoPruebaNosotros">
                    <span class="esquinaImg1Nosotros"><p>Capacitación Ejecutiva</p></span> 
                </div>
            </div>
            <div class="col-sm-4 thumbnail">
                <img src="assets/img/nosotros/educacion-continua-icon.png">
                <div class="textoPruebaNosotros">
                    <span class="esquinaImg1Nosotros"><p>Educación Continua</p></span> 
                </div>
            </div>
            <div class="col-sm-4 thumbnail">
                <img src="assets/img/nosotros/consultoria-icon.png">
                <div class="textoPruebaNosotros">
                    <span class="esquinaImg1Nosotros"><p>Consultoría</p></span> 
                </div>
            </div>
        </div>
    </div>
    
	<div class="m">
		<div class="container-fluid">
			<div class="mmmmm">
				<div class="col-sm-12 nosotrosImagen">
					<center><img  src="assets/img/nosotros/dospuntos-gpc.png" class="img-responsive"></center> 
					<div class="sec3Texto">
						<p>TALISIS: LEARNING & DEVELOPMENT, DEDICADOS A CREAR PROGRAMAS A LA MEDIDA DE CADA CLIENTE.</p>
					</div>
				</div>
	  		</div>
	    </div>	
	</div>
    <div class="sec-texto">
        <div class="container">
            <div class="col-sm-10 col-sm-offset-1">
                <span class="sec4Texto">
                    <p>Nuestro servicios cuentan con una metodología que permite garantizar la calidad de los programas desarrollados para tu empresa</p>
                </span>
            </div>
        </div>
    </div>
<!----> 

    <div class="imagenesTrasladadas">
        <div class="container-fluid">
            <div class="col-sm-12">
                <div class="col-sm-6">
                    <div class="textoTraslado">
                        <p>GRUPOS A LA MEDIDA</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <img src="assets/img/nosotros/1.jpg" class="img-responsive">
                </div>
                <div class="col-sm-6 col-sm-push-6">
                    <div class="textoTraslado">
                        <p>ENFOQUE PRÁCTICO</p>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                    <img src="assets/img/nosotros/2.jpg" class="img-responsive">
                </div>
                <div class="col-sm-6">
                    <div class="textoTraslado">
                        <p>DESARROLLO DE PROYECTOS</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <img src="assets/img/nosotros/3.jpg" class="img-responsive">
                </div>
                <div class="col-sm-6 col-sm-push-6">
                    <div class="textoTraslado">
                        <p>HORARIOS FLEXIBLES</p>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-pull-6">
                    <img src="assets/img/nosotros/4.jpg" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
<!---->
    <div class="newNosotrosN">
        <div class="container">
            <div class="col-sm-6 col-sm-offset-3">
                <span class="sec4Texto">
                    <p>Creamos soluciones de capacitación, educación o consultoría de acuerdo a tu empresa</p>
                </span>
            </div>
        </div>      
    </div>  
<!---->
    <div class="secImgTextoNumero">
        <div class="container">
            <div class="col-sm-12">
                <div class="col-sm-1"></div>
                <div class="col-sm-2">
                    <img src="assets/img/nosotros/1N.jpg" class="img-responsive">
                </div>
                <div class="col-sm-2">
                    <img src="assets/img/nosotros/2N.jpg" class="img-responsive">
                </div>
                <div class="col-sm-2">
                    <img src="assets/img/nosotros/3N.jpg" class="img-responsive">
                </div>
                <div class="col-sm-2">
                    <img src="assets/img/nosotros/4N.jpg" class="img-responsive">
                </div>
                <div class="col-sm-2">
                    <img src="assets/img/nosotros/5N.jpg" class="img-responsive">
                </div>
                <div class="col-sm-1"></div>
            </div>
        </div>
    </div>
<!---->
	<div class="sec4NosotrosNos">
        <div class="container">
            <div class="col-sm-10 col-sm-offset-1">
                <span class="sec4Texto">
                    <p>Talisis: Learning & Development cuenta con varios socios estratégicos que le permiten entregar un servicio de alta calidad a las empresas y sus futuros estudiantes.</p>
                </span>
            </div>
        </div>
    	<div class="container-fluid">
    		<div class="imagenesCirculo">
    			<div class="col-sm-1"></div>
    			<div class="col-sm-2">
    				<div class="imgOverlay" id="myImage" onclick="changeImage()">
    				</div>
    				<div class="col-sm-10">
    					<span class="cirTexto"><br>
    						<p>Kinedu</p>
    					</span>
    					<span class="cirTexto1">
    						<p>
    							Kinedu empowers parent to encourage their babies' development through insights on early childhood and ideas for experiences and interactions between adults and children. Kinedu's main product is an app that provides fun and age-based daily activities for parents to do with their children, nurturing closer relationships and fostering healthy brain building.
    						</p>
    					</span>
    					<span class="cirTexto1">
    						<p><a href="http://www.kinedu.com/es" target="_blank" >www.kinedu.com</a></p> 
    					</span>
    				</div>
    			</div>
    			<div class="col-sm-2">
    				<div class="imgOverlay1" id="myImage1" onclick="changeImage1()"></div>
    				<div class="col-sm-10">
    					<span class="cirTexto"><br>
    						<p>Advenio</p>
    					</span>
    					<span class="cirTexto1">
    						<p>
    							Talisis: Learning & Development  cuenta con varios socios estratégicos que le permiten entregar un servicio de alta calidad a las empresas y sus futuros estudiantes.
    						</p>
    					</span>
    					<span class="cirTexto1">
    						<p><a href="https://advenio.mx/" target="_blank">www.advenio.mx</a></p> 
    					</span>
    				</div>
    			</div>
    			<div class="col-sm-2">
    				<div class="imgOverlay2" id="myImage2" onclick="changeImage2()"></div>
    				<div class="col-sm-10">
    					<span class="cirTexto"><br>
    						<p>U-ERRE</p>
    					</span>
    					<span class="cirTexto1">
    						<p>
    							Talisis: Learning & Development  cuenta con varios socios estratégicos que le permiten entregar un servicio de alta calidad a las empresas y sus futuros estudiantes.
    						</p>
    					</span>
    					<span class="cirTexto1">
    						<p><a href="http://www.u-erre.mx/" target="_blank">www.u-erre.mx</a></p> 
    					</span>
    				</div>
    			</div>
    			<div class="col-sm-2">
    				<div class="imgOverlay3" id="myImage3" onclick="changeImage3()"></div>
    				<div class="col-sm-10">
    					<span class="cirTexto"><br>
    						<p>UNID</p>
    					</span>
    					<span class="cirTexto1">
    						<p>
    							Talisis: Learning & Development  cuenta con varios socios estratégicos que le permiten entregar un servicio de alta calidad a las empresas y sus futuros estudiantes.
    						</p>
    					</span>
    					<span class="cirTexto1">
    						<p><a href="http://www.unid.edu.mx/" target="_blank">www.unid.edu.mx</a></p> 
    					</span>
    				</div>
    			</div>
    			<div class="col-sm-2">
    				<div class="imgOverlay4" id="myImage4" onclick="changeImage4()"></div>
    				<div class="col-sm-10">
    					<span class="cirTexto"><br>
    						<p>IESALUD</p>
    					</span>
    					<span class="cirTexto1">
    						<p>
    							Talisis: Learning & Development  cuenta con varios socios estratégicos que le permiten entregar un servicio de alta calidad a las empresas y sus futuros estudiantes.
    						</p>
    					</span>
    					<span class="cirTexto1">
    						<p><a href="http://www.iesalud.edu.mx/" target="_blank">www.iesalud.edu.mx</a></p> 
    					</span>
    				</div>
    			</div>

    			<div class="col-sm-1"></div>
    		</div>
    	</div>
    </div>
	<div class="sec3Anti">
		
	</div>

    <div class="sec3Contacto"  id="contacto">
        <div class="container-fluid">
            <div class="col-sm-12 tituloPreg">
                <p><span class="tituloPreg1">¿Estás listo para hacer más productiva a tu empresa?</span> </p>
                <p><span class="tituloPreg2">Ingresa tus datos de contacto en el siguiente formulario y nosotros nos pondremos en contacto</span></p>
            </div>
        </div>
        <div class="container">
            <div class="formulario" id="contactoN">
                <form action="send.php" method="post">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="col-sm-5">
                            <input type="text" id="fname" required="required" name="nombre" placeholder="Nombre Completo">
                        </div>
                        <div class="col-sm-5 col-sm-offset-1">
                            <input type="text" id="fname" required="required" name="email" placeholder="Email">
                        </div>
                        </div>
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="col-sm-5">
                            <input type="text" id="fname" required="required" name="telefono" placeholder="Teléfono (10 dígitos)">
                        </div>
                        <div class="col-sm-5 col-sm-offset-1">
                            <input type="text" id="fname" required="required" name="empresa" placeholder="Empresa">
                        </div>
                    </div>
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="col-sm-11">
                            <input type="text" name="comentario" required="required" id="fname" placeholder="Comentarios ..">
                        </div>
                    </div>
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="col-sm-2 col-sm-offset-5">
                            <input type="submit" value="ENVIAR" class="boton">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>

<!---------------------------------------------------------------------------->
    <!-- Page footer section -->
    <?php include_once('src/partial/footer2.php'); ?>

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Bootstrap -->
<script src="assets/lib/bootstrap.min.js"></script>
<!-- Bootbox -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
</body>
</html>


<script>
function changeImage() {
    var image = document.getElementById('myImage');
    if (image.src.match("on")) {
        image.src = "assets/img/nosotros/kinedu-off1.png";
    } else {
        image.src = "assets/img/nosotros/kinedu-on-img1.png";
    }
}

function changeImage1() {
    var image = document.getElementById('myImage1');
    if (image.src.match("on")) {
        image.src = "assets/img/nosotros/advenio-off-img.png";
    } else {
        image.src = "assets/img/nosotros/advenio-on-img.png.png";
    }
}

function changeImage2() {
    var image = document.getElementById('myImage2');
    if (image.src.match("on")) {
        image.src = "assets/img/nosotros/uerre-off.png";
    } else {
        image.src = "assets/img/nosotros/uerre-on-img.png";
    }
}

function changeImage3() {
    var image = document.getElementById('myImage3');
    if (image.src.match("on")) {
        image.src = "assets/img/nosotros/unid-off.png";
    } else {
        image.src = "assets/img/nosotros/unid-on-img.png";
    }
}

function changeImage4() {
    var image = document.getElementById('myImage4');
    if (image.src.match("on")) {
        image.src = "assets/img/nosotros/iesalud-off.png";
    } else {
        image.src = "assets/img/nosotros/iesalud-on-img.png";
    }
}
</script>