<!DOCTYPE html>
<html lang="es" ng-app="MetodikaTI">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- SEO Metatags -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Metodikat TI">

    <!-- Structured data -->
    <?php include_once('src/partial/seo/structured-data.php'); ?>

    <!-- Googlebot -->
    <?php include_once('src/partial/seo/googlebot.php'); ?>

    <!-- Facebook Pixel Code -->
    <?php include_once('src/partial/seo/fb-pixel.php'); ?>

    <!-- Title -->
    <title>
        Talisis: Learning & Development - Nosotros
    </title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <!-- Preloader -->
    <link href="assets/css/preloader.css" rel="stylesheet" />
    <script src="assets/js/preloader.js"></script>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- CSS -->
    <link href="assets/css/main.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/bxslider/jquery.bxslider.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- AngularJS -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js"></script>
    <script src="app/app.js"></script>

    <!-- Analytics code -->
    <?php include_once('src/partial/seo/analytics.php'); ?>
</head>

<body>
<!-- Preloader -->
<div class="preloader"></div>

<!-- All page content goes inside this div -->
<div id="pageContent" class="main-wrapper">

    <!-- Page header section -->
    <?php include_once('src/partial/header2.php'); ?>

    <!-- Page content section -->
    <div class="top_header bg_educacion_ejecutiva">
        <div class="container text-center">
            <p class="titulo_interior">CAPACITACIÓN QUE HACE <br><big-font>CRECER</big-font> TU EMPRESA</p>
        </div> 
    </div>


    <div class="soluciones_corporativas section_content">

        <div class="container">
            <p class="title_content">Soluciones Corporativas de Aprendizaje</p>
            <p class="description text-center">Talisis tiene como objetivo fortalecer el talento de tu empresa, por lo que nos especializamos en crear planes de capacitación enfocadas en el desarrollo de tus colaboradores.</p>
            <p class="subtitle_content">Brindamos capacitación en las siguientes áreas:</p>
            <br>

            <div class="row">
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/negocios-educacion-ejecutiva-icon.png" />
                    <p class="title_area">NEGOCIOS</p>
                </div>
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/comercial-educacion-ejecutiva-icon.png" />
                    <p class="title_area">COMERCIAL</p>
                    <p class="little_area">VENTAS Y MERCADOTECNIA</p>
                </div>
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/calidad-educacion-ejecutiva-icon.png" />
                    <p class="title_area">CALIDAD Y MEJORA CONTINUA</p>
                </div>
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/comercial-educacion-ejecutiva-icon.png" />
                    <p class="title_area">RH Y EDUCACIÓN</p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/finanzas.png" />
                    <p class="title_area">FINANZAS</p>
                </div>
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/construccion-educacion-ejecutiva-icon.png" />
                    <p class="title_area">INDUSTRIA DE LA CONSTRUCCIÓN</p>
                </div>
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/produccion-educacion-ejecutiva-icon.png" />
                    <p class="title_area">PRODUCCIÓN</p>
                </div>
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/cadena-suministro-educacion-ejecutiva-icon.png" />
                    <p class="title_area">CADENA DE SUMINISTRO</p>
                </div>
            </div>


            <div class="row">
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/medio-ambiente-educacion-ejecutiva-icon.png" />
                    <p class="title_area">SEGURIDAD INDUSTRIAL Y MEDIO AMBIENTE</p>
                </div>
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/sistemas-educacion-ejecutiva-icon.png" />
                    <p class="title_area">SISTEMAS</p>
                </div>
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/area-legal-educacion-ejecutiva-icon.png" />
                    <p class="title_area">ÁREA LEGAL</p>
                </div>
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/gastronomia-turismo-educacion-ejecutiva-icon.png" />
                    <p class="title_area">GASTRONOMÍA Y TURISMO</p>
                </div>
            </div>


            <div class="row">
                <div class="col-md-3 servicio">
                    <img src="assets/img/educacion_ejecutiva/idiomas-educacion-ejecutiva-icon.png" />
                    <p class="title_area">IDIOMAS</p>
                </div>
            </div>


        </div>

        <br><br>
        <br><br>

        <div class="container">
            <p class="subtitle_content">Conoce nuestros planes</p>
            <br><br>
            <div class="w-100">
                <div class="text-center min-height-150">
                    <div class="col-5">
                        <p class="subtitle_content_blue">Prepa Empresa</p>
                        <p class="content_little">2 años.</p>
                    </div>
                    <div class="col-5">
                        <p class="subtitle_content_blue">Licenciatura</p>
                        <p class="content_little">3 años.</p>
                    </div>
                    <div class="col-5">
                        <p class="subtitle_content_blue">Ingeniería</p>
                        <p class="content_little">3 años 4 meses.</p>
                    </div>
                    <div class="col-5">
                        <p class="subtitle_content_blue">Posgrado</p>
                        <p class="content_little">2 años 4 meses.</p>
                    </div>
                    <div class="col-5">
                        <p class="subtitle_content_blue">Especialidades</p>
                        <p class="content_little">1 a 1.5 años según la especialidad.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>


    </div>

</div>



<div class="sec3Contacto"  id="contacto">
        <div class="container-fluid">
            <div class="col-sm-12 tituloPreg">
                <p><span class="tituloPreg1">¿Estás listo para hacer más productiva a tu empresa?</span> </p>
                <p><span class="tituloPreg2">Ingresa tus datos de contacto en el siguiente formulario y nosotros nos pondremos en contacto</span></p>
            </div>
        </div>
        <div class="container">
            <div class="formulario" id="contactoN">
                <form action="send.php" method="post">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="col-sm-5">
                            <input type="text" id="fname" required="required" name="nombre" placeholder="Nombre Completo">
                        </div>
                        <div class="col-sm-5 col-sm-offset-1">
                            <input type="text" id="fname" required="required" name="email" placeholder="Email">
                        </div>
                        </div>
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="col-sm-5">
                            <input type="text" id="fname" required="required" name="telefono" placeholder="Teléfono (10 dígitos)">
                        </div>
                        <div class="col-sm-5 col-sm-offset-1">
                            <input type="text" id="fname" required="required" name="empresa" placeholder="Empresa">
                        </div>
                    </div>
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="col-sm-11">
                            <input type="text" name="comentario" required="required" id="fname" placeholder="Comentarios ..">
                        </div>
                    </div>
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="col-sm-2 col-sm-offset-5">
                            <input type="submit" value="ENVIAR" class="boton">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>

<!---------------------------------------------------------------------------->
    <!-- Page footer section -->
    <?php include_once('src/partial/footer2.php'); ?>

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Bootstrap -->
<script src="assets/lib/bootstrap.min.js"></script>
<!-- Bootbox -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
</body>
</html>


<script>
function changeImage() {
    var image = document.getElementById('myImage');
    if (image.src.match("on")) {
        image.src = "assets/img/nosotros/kinedu-off1.png";
    } else {
        image.src = "assets/img/nosotros/kinedu-on-img1.png";
    }
}

function changeImage1() {
    var image = document.getElementById('myImage1');
    if (image.src.match("on")) {
        image.src = "assets/img/nosotros/advenio-off-img.png";
    } else {
        image.src = "assets/img/nosotros/advenio-on-img.png.png";
    }
}

function changeImage2() {
    var image = document.getElementById('myImage2');
    if (image.src.match("on")) {
        image.src = "assets/img/nosotros/uerre-off.png";
    } else {
        image.src = "assets/img/nosotros/uerre-on-img.png";
    }
}

function changeImage3() {
    var image = document.getElementById('myImage3');
    if (image.src.match("on")) {
        image.src = "assets/img/nosotros/unid-off.png";
    } else {
        image.src = "assets/img/nosotros/unid-on-img.png";
    }
}

function changeImage4() {
    var image = document.getElementById('myImage4');
    if (image.src.match("on")) {
        image.src = "assets/img/nosotros/iesalud-off.png";
    } else {
        image.src = "assets/img/nosotros/iesalud-on-img.png";
    }
}
</script>