<!DOCTYPE html>
<html lang="es" ng-app="MetodikaTI">
<head>
    <meta charset="utf-8">
    <meta name="description" content="Talisis ayudamos a enriquecer el talento de alta calidad">
    <meta name="keywords" content="de acuerdo a las decesidades especificas de tu empresa.">
    <meta name="author" content="Produccion a tu empresa">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- SEO Metatags -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Metodikat TI">

    <!-- Structured data -->
    <?php include_once('src/partial/seo/structured-data.php'); ?>

    <!-- Googlebot -->
    <?php include_once('src/partial/seo/googlebot.php'); ?>

    <!-- Facebook Pixel Code -->
    <?php include_once('src/partial/seo/fb-pixel.php'); ?>

    <!-- Title -->
    <title>Talisis: Learning & Development</title>
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Preloader -->
    <link href="assets/css/preloader.css" rel="stylesheet" />
    <script src="assets/js/preloader.js"></script>

    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- CSS -->
    <link href="assets/css/main.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/bxslider/jquery.bxslider.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/js/modal-video/css/modal-video.min.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- AngularJS -->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.2/angular.min.js"></script>-->
<!--    <script src="app/app.js"></script>-->

    <!-- Analytics code -->
    <?php include_once('src/partial/seo/analytics.php'); ?>
</head>

<body>
<!-- Preloader -->
<div class="preloader"></div>

<!-- All page content goes inside this div -->
<div id="pageContent" class="main-wrapper">

    <!-- Page header section -->
    <?php include_once('src/partial/header.php'); ?>

    <!-- Page content section -->
<!--    <div class="h-sec1-banner">-->
<!--        <ul class="bxslider-home">-->
<!--            <li class="h-sec1">-->
<!--                 <div class="col-sm-12 texto">-->
<!--                     <span class="titulo1"><p>TALISIS</p> </span>-->
<!--                     <span class="titulo2"><p>LEARNING AND DEVELOPMENT</p> </span>-->
<!--                     <span class="titulo3"><p>DRIVE CHANGE TO EMPOWER YOUR BUSINESS</p> </span>-->
<!--                 </div>          -->
<!--            </li>-->
<!--            <li class="h-sec2">-->
<!--                 <div class="col-sm-12 texto">-->
<!--                     <span class="titulo1"><p>TALISIS</p> </span>-->
<!--                     <span class="titulo2"><p>LEARNING AND DEVELOPMENT</p> </span>-->
<!--                     <span class="titulo3"><p>DRIVE CHANGE TO EMPOWER YOUR BUSINESS</p> </span>-->
<!--                 </div>          -->
<!--            </li>-->
<!--            <li class="h-sec3">-->
<!--                 <div class="col-sm-12 texto">-->
<!--                     <span class="titulo1"><p>TALISIS</p> </span>-->
<!--                     <span class="titulo2"><p>LEARNING AND DEVELOPMENT</p> </span>-->
<!--                     <span class="titulo3"><p>DRIVE CHANGE TO EMPOWER YOUR BUSINESS</p> </span>-->
<!--                 </div>          -->
<!--            </li>-->
<!--        </ul>-->
<!--    </div>-->

<!--    <div class="bg-top-video js-modal-btn" data-video-id="tHhRWBw53J8">-->
<!---->
<!--    </div>-->

    <div class="section-top-video" >
        <div class="container text-center">
            <p class="title">TALISIS</p>
            <p class="subtitle">LEARNING AND DEVELOPMENT</p>
            <p class="little_subtitle">DRIVE CHANGE TO EMPOWER YOUR BUSINESS</p>
            <img src="assets/img/play_button.png" class="play_button  js-modal-btn" data-video-id="tHhRWBw53J8"/>
        </div>
    </div>


    <div class="sec1" id="sec2Nuevo">
        <div class="container-fluid">
            <!----> 
            <div class="col-sm-4 square_text">
                <div class="imgPrueba">
                    <div class="textoPrueba">
                        <span class="esquinaImg1"><p>Capacitación Ejecutiva</p></span> 
                        <br>
                    </div>
                    <div class="imgOverlay15" id="myImage10" onclick="changeImage10()">
                        <div class="producto prod-first" id="myImage10" onclick="changeImage10()">  
                            <div class="mask">  
                                <div class="ImgCambios">
                                    <span class="tituloOver"><h2>Capacitación Ejecutiva</span></h2> </span> 
                                    <p>
                                        Desarrollamos el modelo de capacitación que tu empresa necesita para lograr sus objetivos.
                                    </p>
                                    <p> 
                                        Nuestro amplio conocimiento y experiencia en capacitación ejecutiva nos dan el respaldo y la confianza para ofrecerte los mejores modelos de capacitación.
                                    </p>
                                    <a class="see_more" href="educacion_ejecutiva.php">Ver más [+]</a>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4 square_text">
                <div class="imgPrueba2">
                    <div class="textoPrueba">
                        <span class="esquinaImg1"><p>Educación Continua</p></span> 
                        <br>
                    </div>
                    <div class="producto prod-first" id="myImage11" onclick="changeImage11()"> 
                        <div class="mask1">  
                            <span class="tituloOver"><h2>Educación Continua</h2> </span>
                            <p>
                                Contamos con los mejores planes de Educación Continua que necesitas para fortalecer el talento de tu empresa. 
                            </p>
                            <a class="see_more" href="educacion_continua.php">Ver más [+]</a>
                        </div>  
                    </div>
                </div>
            </div>

            <div class="col-sm-4 square_text">
                <div class="imgPrueba3">
                    <div class="textoPrueba">
                        <span class="esquinaImg1"><p>Consultoría</p></span> 
                        <br>
                    </div>
                    <div class="producto prod-first" id="myImage12" onclick="changeImage12()">
                        <div class="mask2">  
                            <span class="tituloOver"><h2>Consultoría</h2> </span>
                            <p>
                                Con la consultoría que provee Talisis: Learning & Development permite a tu empresa acelerar el proceso de llegar a sus objetivos.
                            </p>
                        </div>  
                    </div>
                </div>
            </div>


            <div class="col-sm-12">
                <div class="producto prod-first1">  
                    <img src="assets/img/home/queestalisis-img.jpg" class="img-responsive">
                    <div class="mask3">  
                        <span class="tituloOver"><h2>¿Qué es Talisis: Learning & Development ?</h2></span>
                        <p>
                            Ayudamos a enriquecer el talento por medio de experiencias de alta calidad de capacitación donde se desarrollan las habilidades y competencias del ser. 
                        </p>
                        <p>
                            Cada modelo de capacitación se crea de acuerdo a las necesidades específicas de tu empresa. 
                        </p>
                    </div>  
                </div>
            </div>
            <div class="col-sm-6"></div>
        </div>
    </div>

    <div class="marcas">
        <div class="container">
            <div class="col-md-12">
                <p class="title_content">Alianzas estratégicas que confían en nosotros:</p>
                <br>
            </div>
            <div class="col-md-12">
                <div class="slider_marcas">
                    <div><img src="assets/img/home/clientes/arca-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/fep-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/yeseramty-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/grupofamsa-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/imss-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/teleperformance-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/cemex-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/lambi-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/zinc-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/infosys-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/axa-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/coflex-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/monterreygob-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/cataforesis-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/enerya-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/chedraui-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/delphi-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/multimedios-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/vitro-id.jpg"></div>
                    <div><img src="assets/img/home/clientes/benavides-id.jpg"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="sec2">
        <div class="container">
            <div class="col-md-12">
                <p class="title_content">CONOCE EL ALCANCE DE NUESTRO PROYECTOS</p>
            </div>
            <div class="col-sm-6 circulosEspacio">
                <div class="col-sm-12">
                    <div class="col-sm-4 col-xs-6">
                        <div class="counter" data-count="450">0</div>
                        <div class="col-sm-8 col-sm-offset-1 espacioCircular">
                            <p><span class="count1">Profesores y Coaches</span></p>  
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <div class="counter" data-count="2000">0</div>
                         <div class="col-sm-10  espacioCircular">
                             <p><span class="count1">Colaboradores en Transformación</span></p>
                         </div>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <div class="counter" data-count="30">0</div>
                        <div class="col-sm-6 col-sm-offset-2 espacioCircular">
                            <p>
                                <span class="count1">Alianzas</span>
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="col-sm-4 col-xs-6">
                        <div class="counter" data-count="60">0</div>
                        <div class="col-sm-11 espacioCircular">
                            <p>
                                <span class="count1">Centros de Aprendizaje y Desarrollo</span>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-xs-6">
                        <div class="counter" data-count="150">0</div>
                        <div class="col-sm-10 col-sm-offset-2 espacioCircular">
                            <p>
                                <span class="count1">Soluciones Diseñadas</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <img src="assets/img/home/mapa-america-mexico.gif" class="img-responsive">
            </div>
        </div>
    </div>

    <div class="sec3Contacto" id="contacto">
        <div class="container-fluid">
            <div class="col-sm-12 tituloPreg">
                <p><span class="tituloPreg1">¿Estás listo para hacer más productiva a tu empresa?</span> </p>
                <p><span class="tituloPreg2">Ingresa tus datos de contacto en el siguiente formulario y nosotros nos pondremos en contacto</span></p>
            </div>
        </div>
        <div class="container">
            <div class="formulario">
                <form action="send.php" method="post">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <input type="text" id="fname" required="required" name="nombre" placeholder="Nombre Completo">
                        </div>
                        <div class="col-sm-6">
                            <input type="text" id="fname" required="required" name="email" placeholder="Email">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <input type="text" id="fname" required="required" name="Teléfono" placeholder="Teléfono (10 dígitos)">
                        </div>
                        <div class="col-sm-6">
                                <input type="text" id="fname" required="required" name="empresa" placeholder="Empresa">  
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <select required="required" name="estado">
                                <option value="">Estado de procedencia</option>
                                <option value="Aguascalientes" >Aguascalientes</option>
                                <option value="Baja California" >Baja California</option>
                                <option value="Baja California Sur" >Baja California Sur</option>
                                <option value="Campeche" >Campeche</option>
                                <option value="Chiapas" >Chiapas</option>
                                <option value="Chihuahua" >Chihuahua</option>
                                <option value="Ciudad de México" >Ciudad de México</option>
                                <option value="Coahuila" >Coahuila</option>
                                <option value="Colima" >Colima</option>
                                <option value="Durango" >Durango</option>
                                <option value="Guanajuato" >Guanajuato</option>
                                <option value="Guerrero" >Guerrero</option>
                                <option value="Hidalgo" >Hidalgo</option>
                                <option value="Jalisco" >Jalisco</option>
                                <option value="México" >México</option>
                                <option value="Michoacán" >Michoacán</option>
                                <option value="Morelos" >Morelos</option>
                                <option value="Nayarit" >Nayarit</option>
                                <option value="Nuevo León" >Nuevo León</option>
                                <option value="Oaxaca" >Oaxaca</option>
                                <option value="Puebla" >Puebla</option>
                                <option value="Querétaro" >Querétaro</option>
                                <option value="Quintana Roo" >Quintana Roo</option>
                                <option value="San Luis Potosí" >San Luis Potosí</option>
                                <option value="Sinaloa" >Sinaloa</option>
                                <option value="Sonora" >Sonora</option>
                                <option value="Tabasco" >Tabasco</option>
                                <option value="Tamaulipas" >Tamaulipas</option>
                                <option value="Tlaxcala" >Tlaxcala</option>
                                <option value="Veracruz" >Veracruz</option>
                                <option value="Yucatán" >Yucatán</option>
                                <option value="Zacatecas" >Zacatecas</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                            <textarea type="text" name="comentario" required="required" id="fname" placeholder="Comentarios .."></textarea>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-2 col-sm-offset-5">
                            <input type="submit" value="Enviar" class="boton">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>

<script type="text/javascript">
    $('.counter').each(function() {
  var $this = $(this),
      countTo = $this.attr('data-count');
  
  $({ countNum: $this.text()}).animate({
    countNum: countTo
  },

  {

    duration: 1000,
    easing:'linear',
    step: function() {
      $this.text(Math.floor(this.countNum));
    },
    complete: function() {
      $this.text(this.countNum);
      //alert('finished');
    }

  });  
  
  

});
</script>
<!---------------------------------------------------------------------------->
    <!-- Page footer section -->
    <?php include_once('src/partial/footer.php'); ?>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src="assets/js/jquery.bxslider.min.js"></script>
<!-- Bootstrap -->
<script src="assets/lib/bootstrap.min.js"></script>
<!-- Bootbox -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
<script src="assets/js/modal-video/js/jquery-modal-video.js"></script>

</body>
</html>
<script>
function changeImage15() {
    var image = document.getElementById('myImage15');
    if (image.src.match("on")) {
        image.src = "assets/img/home/ejecutiva.jpg";
    } else {
        image.src = "assets/img/home/ejecutiva-on.jpg";
    }
}


function changeImage10() {
    var image = document.getElementById('myImage10');
    if (image.src.match("on")) {
        image.src = "assets/img/home/ejecutiva.jpg";
    } else {
        image.src = "assets/img/home/ejecutiva-on.jpg";
    }
}

function changeImage11() {
    var image = document.getElementById('myImage11');
    if (image.src.match("on")) {
        image.src = "assets/img/home/econtinua.jpg";
    } else {
        image.src = "assets/img/home/econtinua-on.jpg";
    }
}

function changeImage12() {
    var image = document.getElementById('myImage12');
    if (image.src.match("on")) {
        image.src = "assets/img/home/consultoria.jpg";
    } else {
        image.src = "assets/img/home/consultoria-on.jpg";
    }
}

</script>
<script> 
$(document).ready(function(){

    $('.bxslider-home').bxSlider({
        auto: true,
    });

    var slider_width = $(".marcas .container").width() / 3;
    $('.slider_marcas').bxSlider({
        minSlides: 1,
        maxSlides: 3,
        slideWidth: slider_width,
        slideMargin: 0,
        auto: true,
    });

    $(".js-modal-btn").modalVideo();
});
</script>