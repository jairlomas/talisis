<footer class="sec-footer">
	<div class="container-fluid">
		<div class="col-sm-12">
			<div class="col-sm-2 col-sm-offset-2"><br>
				<ul class="nav footer-nav">
					<li><p><a href="index.php"> <span class="textFooter">HOME</span></a></p></li>
					<li><p><a href="index.php#sec2Nuevo" id="#sec2Nuevo"><span class="textFooter">TALISIS: LEARNING & DEVELOPMENT</span></a></p></li>
					<li><p><a href="#contacto" id="#contacto"> <span class="textFooter">CONTACTO</span></a></p></li>
                    <li><p><a href="https://portal.office.com"> <span class="textFooter">EMAIL TALISIS</span></a></p></li>
                </ul>
			</div> 
			<div class="col-sm-3">
				<div class="footer-texto"><br>
					<p>
                        TELÉFONO: <a href="tel:+52 123-456-7890">+ 52 123-456-7890 </a>
					</p>
					<p>
						OFICINA CENTRAL:
					</p>
					<p>
						Matamoros #430 Pte. 
						Col. Centro, Monterrey, N. L.
					</p>
					<p> Email: <a href="mailto:infold@talisis.com"> infold@talisis.com</a> </p>

				</div>
				
			</div>
			<div class="col-sm-3 col-sm-offset-1"><br>
				<div class="col-sm-2 col-xs-4">
					<a href="#"><img src="assets/img/fb-off-icon.png" class="img-responsive"></a>
				</div>
				<div class="col-sm-2 col-xs-4">
					<a href="#"><img src="assets/img/in-off-icon.png" class="img-responsive"></a>
				</div>
				<div class="col-sm-2 col-xs-4">
					<a href="#"><img src="assets/img/gplus-off-icon.png" class="img-responsive"></a>
				</div>
				<div class="col-sm-12">
					<br><br><img src="assets/img/drivechange-footer-id.png" class="img-responsive">
				</div>
			</div>
		</div>
	</div>
	<div class="line-footer">
		<div class="container">
			<div class="col-sm-8">
				<br>
				 © 2017 Talisis: Learning & Development | <a href="http://futurite.com/" target="_blank"><span class="futurite">Futurite</span> </a>   -  <a href="http://www.metodika.mx/" target="_blank"><span class="metodika">Metodika</span></a>
			</div>
		</div>
	</div>
</footer>